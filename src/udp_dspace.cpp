#include <ros/ros.h>
#include <std_msgs/Float32MultiArray.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <geometry_msgs/Quaternion.h>
#include <cmath>

#include "boost/asio.hpp"
using namespace boost::asio;

geometry_msgs::Quaternion ori;
geometry_msgs::Vector3 ins_vel;
std_msgs::Float32MultiArray::Ptr cone_cb(new std_msgs::Float32MultiArray);

bool vel_bool = false;
bool ori_bool = false;
bool cone_bool = false;

uint32_t vel[1];
uint32_t yaw[1];

void ins_vel_cb (const geometry_msgs::Vector3StampedConstPtr& raw_msg)
{
	ins_vel = raw_msg->vector;
	if(ins_vel.y != 0) vel_bool = true;
  else if(ins_vel.y == 0) vel_bool = false;

}

void cone_depth_cb (const std_msgs::Float32MultiArray::ConstPtr& raw_msg)
{
	cone_cb->data = raw_msg->data;
	cone_bool = true;
	//ROS_INFO("%i",(int)raw_msg->data[0]);

}
void ori_cb (const sensor_msgs::ImuConstPtr& raw_msg)
{
	ori = raw_msg->orientation;
	ori_bool = true;
	//ROS_INFO("%i",(int)raw_msg->orientation.w);

}

int main( int argc, char** argv )
{
	ros::init(argc, argv, "udp_conn");
	ros::NodeHandle n;
	ros::Subscriber sub_ins_vel = n.subscribe<geometry_msgs::Vector3Stamped>("/vectornav/Velocity",3, ins_vel_cb);
	ros::Subscriber sub_cone_pos = n.subscribe<std_msgs::Float32MultiArray>("/unrotated",1, cone_depth_cb);
	ros::Subscriber sub_ori = n.subscribe<sensor_msgs::Imu>("/vectornav/IMU",1, ori_cb);
		
  ros::Rate r(20);

//_________________________UDP________________________
	io_service io_service;
 	//erstellt einen Sockel für die Kommunikation
 	ip::udp::socket socket(io_service);

	//erstellt eine Variable in die der Empfänger geschrieben wird
	ip::udp::endpoint remote_endpoint_1, remote_endpoint_2, remote_endpoint_3;
 
	// legt Empfänger fest
	remote_endpoint_1 = ip::udp::endpoint(ip::address::from_string("192.168.0.75"),7101); // 0.0.0.0 = IP, 9000 = PORT -> choose what you want
	remote_endpoint_2 = ip::udp::endpoint(ip::address::from_string("192.168.0.75"),7102);
	remote_endpoint_3 = ip::udp::endpoint(ip::address::from_string("192.168.0.75"),7103);
	//öffnet den Sockel
	socket.open(ip::udp::v4());
	//Error Variable
	boost::system::error_code err;
  while (ros::ok())
  {
	ros::spinOnce();

if (ori_bool && vel_bool && cone_bool)
{

	double siny = 2.0 * (ori.w * ori.z + ori.y * ori.x);
	double cosy = 1.0 * (ori.z * ori.z + ori.y * ori.y);
	yaw[0] = (uint32_t)((atan2(siny, cosy)+2000)*100);

	//calculate velocity
	vel[0] = (uint32_t)((sqrt(ins_vel.x*ins_vel.x + ins_vel.y*ins_vel.y)+2000)*100);

	uint32_t cone[100]; //converting Cones to uint32 data for transmission
	for (int i=0; i<cone_cb->data[0]*3+1;i++)
	{
		cone[i] = (uint32_t)((cone_cb->data[i]+2000)*1000);
	}

	//Senden der Daten
	socket.send_to(buffer(yaw,4), remote_endpoint_1, 0, err); // 8=4*2=4ByteProVariable*2Variablen
	socket.send_to(buffer(vel,4), remote_endpoint_2, 0, err); // 8=4*2=4ByteProVariable*2Variablen
	socket.send_to(buffer(cone_cb->data,(4*(3*30+1))), remote_endpoint_3, 0, err); // 8=4*2=4ByteProVariable*2Variablen
ROS_INFO("%f",((cone_cb->data[0]*3+1)*4));
}
	r.sleep();
  }
// Schließen des Sockels
socket.close();
}

