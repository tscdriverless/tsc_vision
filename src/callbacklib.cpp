void
ins_pose_cb (const geometry_msgs::PointStampedConstPtr& raw_msg)
{
    ins_pose = raw_msg->point;
    pose = true;
}

void 
cone_depth_cb (const std_msgs::Float32MultiArray::ConstPtr& raw_msg)
{
  cone_cb->data = raw_msg->data;
  depth = true;
}

void
vn_imu_cb (const sensor_msgs::ImuConstPtr& raw_msg)
{
  vn_orientation.setX(raw_msg->orientation.x);
  vn_orientation.setY(raw_msg->orientation.y);
  vn_orientation.setZ(raw_msg->orientation.z);
  vn_orientation.setW(raw_msg->orientation.w);
  imu = true;
}

void
vlp_cb (const std_msgs::Float32MultiArray::ConstPtr& raw_msg)
{
  vlp_array->data = raw_msg->data;
  vlp = true;
}

void 
depth_cb (const sensor_msgs::ImageConstPtr& raw_msg)
{ 
  depth_msg->header   = raw_msg->header;
  depth_msg->height   = raw_msg->height;
  depth_msg->width = raw_msg->width;
  depth_msg->encoding = raw_msg->encoding;
  depth_msg->step     = raw_msg->step;
  depth_msg->data = raw_msg->data;

  try
  {
    cv_ptr = cv_bridge::toCvShare(raw_msg);
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
    return;
  }
  if(!zed) zed = true;
}

void 
NnOutput_cb (const std_msgs::Float32MultiArray::ConstPtr& raw_msg)
{
  cone_msg->data = raw_msg->data;
  if(!nn) nn = true;
}

    ros::Subscriber sub_ins_pose = n.subscribe<geometry_msgs::PointStamped>("/vectornav/POS_UTM",1, ins_pose_cb);
    ros::Subscriber sub_cone_depth = n.subscribe<std_msgs::Float32MultiArray>("/map",1, cone_depth_cb);		
    ros::Subscriber sub_imu = n.subscribe<sensor_msgs::Imu> ("/vectornav/IMU", 1, vn_imu_cb);
    ros::Subscriber sub_vlp = n.subscribe<std_msgs::Float32MultiArray> ("/vlp16_unrotated", 1, vlp_cb);
    ros::Subscriber sub_0 = nh.subscribe<std_msgs::Float32MultiArray>("/bndbxs", 1, NnOutput_cb);
    ros::Subscriber sub_1 = nh.subscribe<sensor_msgs::CameraInfo> ("/zed/depth/camera_info", 1, cameraInfo_cb);
    ros::Subscriber sub_2 = nh.subscribe<nav_msgs::Odometry> ("/zed/odom", 1, zed_odom_cb);
    ros::Subscriber sub_3 = nh.subscribe<sensor_msgs::Image> ("/zed/depth/depth_registered", 1, depth_cb);
    ros::Subscriber sub_4 = nh.subscribe<sensor_msgs::Imu> ("/vectornav/IMU", 1, vn_imu_cb);
    ros::Subscriber sub_pose = nh.subscribe<geometry_msgs::PointStamped>("/vectornav/POS_UTM",1, pose_cb);
    ros::Subscriber sub_cones = nh.subscribe<std_msgs::Float32MultiArray>("/unrotated",1, cone_cb);		
    ros::Subscriber sub_quaternion = nh.subscribe<sensor_msgs::Imu> ("/vectornav/IMU", 1, quaternion_cb);