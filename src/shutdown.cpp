/*
 * Copyright (c) 2017, 2018, Eric Gorkow, Team Starcraft e.V., Ilmenau, Germany
 * All rights reserved.
 */

#include <ros/ros.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/PointStamped.h>
#include <sensor_msgs/Image.h>
#include <stdio.h>
#include <stdlib.h>

bool shutdown = false;

// Main void
int main( int argc, char** argv )
{
	// Initialize ROS
	ros::init (argc, argv, "shutdown");
	

	while(ros::ok()){
		if(shutdown){
			system("shutdown -P now");
		}
		ros::spinOnce();
	}
}