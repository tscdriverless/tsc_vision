/*
 * Copyright (c) 2017, 2018, Eric Gorkow, Team Starcraft e.V., Ilmenau, Germany
 * All rights reserved.
 */

#include <ros/ros.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/PointStamped.h>
#include <sensor_msgs/Image.h>
#include <stdio.h>
#include <stdlib.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>

std_msgs::Float32MultiArray zed_cones_old;
std_msgs::Float32MultiArray zed_cones;
std_msgs::Float32MultiArray lidar_cones;

// proof if data isnt empty
bool zed = false;
bool lidar = false;

// zed cones
void
zed_cb (const std_msgs::Float32MultiArray::ConstPtr& raw_msg)
{
    zed_cones.data = raw_msg->data;
    if(!zed)zed = true;
}

void
zed_old_cb (const std_msgs::Float32MultiArray::ConstPtr& raw_msg)
{
    zed_cones_old.data = raw_msg->data;
}

// vlp cones
void
vlp_cb (const std_msgs::Float32MultiArray::ConstPtr& raw_msg)
{
    lidar_cones.data = raw_msg->data;
    if(!lidar)lidar = true;
}

// calc euclid dist and returns index of nn in base array
int
findNearestNeighbour(std::vector<tf::Vector3> base, tf::Vector3 tba)
{
  tf::Vector3 base_cone;
  int index;
  float best = 0;
  int best_index = -1;
  int base_counter = 0;

  for(std::vector<tf::Vector3>::iterator it_base = base.begin(); it_base != base.end(); ++it_base) {
    base_cone = *it_base;
    // dist treshold
    if(best > base_cone.distance(tba) && 1.5 > base_cone.distance(tba)){
      best = base_cone.distance(tba);
      best_index = base_counter;
    }
    base_counter++;   
  }
  return best_index;
}

// Main void
int main( int argc, char** argv )
{
	// Initialize ROS
	ros::init (argc, argv, "sensor_fusion");
    ros::NodeHandle nh;
    ros::Publisher pub_output = nh.advertise<std_msgs::Float32MultiArray>("/combined_cones",1);
    ros::Subscriber sub_zed = nh.subscribe<std_msgs::Float32MultiArray> ("/unrotated_mean", 1, zed_cb);
    ros::Subscriber sub_zed_old = nh.subscribe<std_msgs::Float32MultiArray> ("/unrotated", 1, zed_old_cb);
    ros::Subscriber sub_vlp = nh.subscribe<std_msgs::Float32MultiArray> ("/vlp16_unrotated", 1, vlp_cb);
    ros::Rate r = 5;

	while(ros::ok()){
		if(zed){
            // make data pers
            std_msgs::Float32MultiArray output;
            std_msgs::Float32MultiArray zed_cones_pers = zed_cones;
            std_msgs::Float32MultiArray lidar_cones_pers;
            if(lidar)lidar_cones_pers = lidar_cones;
            if(lidar)lidar_cones.data.clear();
            zed_cones.data.clear();
            // nb of cones in the arrays
            float nb_zed_cones = 0;
            if(zed_cones_pers.data.size() > 0) nb_zed_cones = zed_cones_pers.data[0];
            float nb_lidar_cones = 0.0;
            if(lidar && lidar_cones_pers.data.size() > 0) nb_lidar_cones = lidar_cones_pers.data[0];
            output.data.push_back(nb_zed_cones);
            // vector for lidar cones
            std::vector<tf::Vector3> lidar_cone_vector;
            // fill lidar vector 
            if(lidar && lidar_cones_pers.data.size() > 0){
                for(int i = 1;i<zed_cones_pers.data.size();i=i+2){
                    tf::Vector3 cone(lidar_cones_pers.data[i]+1.6,-lidar_cones_pers.data[i+1],0);
                    lidar_cone_vector.push_back(cone);
                }
            }
            // for every cone of zed_cones find nearest neighbour --> if found update x and y values with lidar ones else do nothing
            for(int i = 1;i<zed_cones_pers.data.size();i=i+4){
                tf::Vector3 cone;
                // confidence treshold
                if(zed_cones_pers.data[i+3] > 0.0) {
                    cone.setX(zed_cones_pers.data[i]);
                    cone.setY(zed_cones_pers.data[i+1]);
                    cone.setZ(0);
                    int index = -1; 
                    if(lidar && lidar_cones_pers.data.size() > 0) index = findNearestNeighbour(lidar_cone_vector, cone);
                    if(index >= 0){
                        cone.setX(lidar_cone_vector.at(index).getX());
                        cone.setY(lidar_cone_vector.at(index).getY());
                        zed_cones_pers.data[i+3] = 1.0;
                        if(index > 0)index=index-1;
                        lidar_cone_vector.erase(lidar_cone_vector.begin()+index);
                    }
                    // fill output with x,y,type,confidence
                    output.data.push_back(cone.getX());
                    output.data.push_back(cone.getY());
                    output.data.push_back(zed_cones_pers.data[i+2]);
                    output.data.push_back(zed_cones_pers.data[i+3]);
                }
            }
            for(int i = 0;i < lidar_cone_vector.size();i++){
                output.data.push_back(lidar_cone_vector.at(i).getX());
                output.data.push_back(lidar_cone_vector.at(i).getY());
                output.data.push_back(4.0);
                output.data.push_back(1.0);
                output.data[0]++;
            }
            pub_output.publish(output);
        }
        r.sleep();
		ros::spinOnce();
	}
}