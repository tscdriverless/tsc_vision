#include <ros/ros.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/MultiArrayDimension.h>

std_msgs::Float32MultiArray parameter_pub_data;
float parameter[3];

int main( int argc, char** argv )
{
	ros::init(argc, argv, "set_dwa_parameter");
	ros::NodeHandle n;
	ros::Publisher pub_parameter = n.advertise<std_msgs::Float32MultiArray>("/DWAParameter",1);
		
  	ros::Rate ros_rate(100);
    
    parameter[0]=0.6; //alpha / heading
    parameter[1]=2; //beta / distance
    parameter[2]=0.2; //gamma / velcur

	//prepare parameter data
	parameter_pub_data.data.clear();
	parameter_pub_data.data.push_back(parameter[0]);
	parameter_pub_data.data.push_back(parameter[1]);
	parameter_pub_data.data.push_back(parameter[2]);

    while (ros::ok())
    {	        
        ros::spinOnce();
		pub_parameter.publish(parameter_pub_data);
	    ros_rate.sleep();
	}
}


