#include <iostream>
#include <string>

#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>
#include <pcl/point_cloud.h>
#include <pcl/console/parse.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>

#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/ModelCoefficients.h>

#include <pcl/filters/extract_indices.h>

#include <pcl/kdtree/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>

#include <Eigen/Dense>

#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <boost/foreach.hpp>

#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <std_msgs/Int32.h>
#include <std_msgs/String.h>

#include <sensor_msgs/PointCloud2.h>

#include <pcl_conversions/pcl_conversions.h>

#include <std_msgs/Float32MultiArray.h>

void pclProcessing(pcl::PointCloud<pcl::PointXYZ>::ConstPtr input, pcl::PointCloud<pcl::PointXYZ> &cloud_nongroundOut, pcl::PointCloud<pcl::PointXYZ> &cloud_groundOut,
pcl::PointCloud<pcl::PointXYZ> &centroidCloudOut, std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> &vectorClusterCloudsOut, double sacThresh)
// constptr as inputs, Pointbla &output as output 
{
    double xLim1 = -3;
    double xLim2 = 5;
    double yLim1 = -4;
    double yLim2 = 4;
    //double sacThresh = 0.05;
    double ecClusterTol = 0.3;
    int ecMinClusterSize = 10;
    int ecMaxClusterSize = 100;

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_nonground(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_ground(new pcl::PointCloud<pcl::PointXYZ>);

    // PassThrough (range) filter for x
    pcl::PassThrough<pcl::PointXYZ> pass1;
    //pass1.setInputCloud(source_cloud);
    pass1.setInputCloud(input);
    pass1.setFilterFieldName("x");
    //pass1.setFilterLimits(1, 12);
    pass1.setFilterLimits(xLim1, xLim2);
    pass1.filter(*cloud);

    // PassThrough (range) filter for y
    pcl::PassThrough<pcl::PointXYZ> pass2;
    pass2.setInputCloud(cloud);
    pass2.setFilterFieldName("y");
    //pass2.setFilterLimits(-10, 10);
    pass2.setFilterLimits(yLim1, yLim2);
    pass2.filter(*cloud);

    ///////////
    pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    //seg.setOptimizeCoefficients (true);
    // Mandatory
    seg.setModelType(pcl::SACMODEL_PLANE);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setDistanceThreshold(sacThresh);
    seg.setInputCloud(cloud);
    seg.segment(*inliers, *coefficients);

    // Index extraction filter for fitted ground plane
    pcl::ExtractIndices<pcl::PointXYZ> extract;
    extract.setInputCloud(cloud);
    extract.setIndices(inliers);
    extract.setNegative(false);
    extract.filter(*cloud_ground);

    // Index extraction filter for rest
    extract.setNegative(true);
    extract.filter(*cloud_nonground);

    // clustering
    // Creating the KdTree object for the search method of the extraction
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
    tree->setInputCloud(cloud_nonground);

    std::vector<pcl::PointIndices> cluster_indices;
    pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;

    ec.setClusterTolerance(ecClusterTol);
    ec.setMinClusterSize(ecMinClusterSize);
    ec.setMaxClusterSize(ecMaxClusterSize);
    ec.setSearchMethod(tree);
    ec.setInputCloud(cloud_nonground);
    ec.extract(cluster_indices);



    int clusterIdx = 0;
    // make vector of point clouds
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> vectorOfClouds;
    pcl::PointCloud<pcl::PointXYZ> centroidCloud;

    // find centroids
    // iterate over vector of PCs
    for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); ++it)
    {
        pcl::PointCloud<pcl::PointXYZ>::Ptr current_cluster(new pcl::PointCloud<pcl::PointXYZ>);
        for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
        current_cluster->points.push_back(cloud_nonground->points[*pit]);
        current_cluster->width = current_cluster->points.size();
        current_cluster->height = 1;
        current_cluster->is_dense = true;

        vectorOfClouds.push_back(current_cluster);

        // compute cluster centroids
        Eigen::Matrix<double, 4, 1> centroid;
        pcl::compute3DCentroid(*current_cluster, centroid);

        pcl::PointXYZ centroidPos;
        centroidPos.x = centroid[0];
        centroidPos.y = centroid[1];
        centroidPos.z = centroid[2];

        centroidCloud.points.push_back(centroidPos);
        centroidCloud.width = centroidCloud.points.size();
        centroidCloud.height = 1;
        centroidCloud.is_dense = true;
        centroidCloud.header.frame_id = "vectornav"; // important so rviz can do the correct transform



        clusterIdx++;
    }

    cloud_nongroundOut = *cloud_nonground;
    cloud_groundOut = *cloud_ground;
    centroidCloudOut = centroidCloud;
    vectorClusterCloudsOut = vectorOfClouds;
}

class SubscribeAndPublish
{
public:
  SubscribeAndPublish()
  {
    //Topic you want to publish
    pub_cloud = n_.advertise<pcl::PointCloud<pcl::PointXYZ>>("/vlp16_cloud", 1);
    pub_array = n_.advertise<std_msgs::Float32MultiArray>("/vlp16_unrotated", 1);

    //Topic you want to subscribe
    sub_ = n_.subscribe("/velodyne_points", 1, &SubscribeAndPublish::callback, this);
  }

  void callback(const pcl::PointCloud<pcl::PointXYZ>::ConstPtr &input)
  {

    //.... do something with the input and generate the output...
    //////////////////////////////////////////////////////////////////////
    double sacThresh=0.05;

    //pcl::PointCloud<pcl::PointXYZ>::Ptr input(new pcl::PointCloud<pcl::PointXYZ>);

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_nonground(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_ground(new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr centroidCloud(new pcl::PointCloud<pcl::PointXYZ>);
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> vectorClusterClouds;
    
    pclProcessing(input, *cloud_nonground, *cloud_ground, *centroidCloud, vectorClusterClouds, sacThresh);

    pcl::PointCloud<pcl::PointXYZ> centroidCloud2; // TODO: this stuff is done here as well as in pclProcessing(), fix that

    std_msgs::Float32MultiArray cone_pose_unrotated;
    float cone_counter = 0;
    cone_pose_unrotated.data.push_back(cone_counter);

    // iterate over clusters
    int currCloudIdx = 0;
    for(std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr>::iterator it = vectorClusterClouds.begin(); it != vectorClusterClouds.end(); ++it)
    {
        
        std::string clusterCloudId = "clusterCloud" + boost::lexical_cast<std::string>(currCloudIdx++);
        
        // compute cluster centroids
        Eigen::Matrix<double, 4, 1> centroid;
        pcl::PointCloud<pcl::PointXYZ>::Ptr currCloud = *it;
        //pcl::compute3DCentroid(*currCloud, centroid);
        pcl::compute3DCentroid(**it, centroid);

        pcl::PointXYZ centroidPos;
        centroidPos.x = centroid[0];
        centroidPos.y = centroid[1];
        centroidPos.z = centroid[2];
        std::string textId = "textCloud" + boost::lexical_cast<std::string>(currCloudIdx++);
        
        // compute axis-aligned bounding boxes (AABB) for each cluster to estimate their size
        Eigen::Map<Eigen::MatrixXf, Eigen::Aligned, Eigen::OuterStride<>> ptMap = currCloud->getMatrixXfMap(3,4,0); // for getting only XYZ coordinates out of PointXYZ use dim=3, stride=4 and offset=0 due to the alignment. 
        Eigen::MatrixXf maxs(1,3);
        Eigen::MatrixXf mins(1,3);
        maxs = ptMap.rowwise().maxCoeff(); // get min and max coeffs (=coordinates) from point matrix
        mins = ptMap.rowwise().minCoeff();
        //std::cout << "cluster " << boost::lexical_cast<std::string>(clusterIdx) << "\n" << maxs.transpose() << "\n" << mins.transpose() << "\n";
        float szX = maxs(0,0)-mins(0,0);
        float szY = maxs(1,0)-mins(1,0);
        float szZ = maxs(2,0)-mins(2,0);

        // conditions for cone / not cone:
        // height < 0.5 m
        // height > 0.2 m -> difficult to enforce if cones are only partially imaged
        // width / length < 0.3 m
        // 

        
        if ((std::sqrt(pow(szX,2)+pow(szY,2)) > 0.4) & (szZ < 0.5)) // make boxes red if not cones
        {
            // not cone
        }
        else
        {
            // cone
            centroidCloud2.points.push_back(centroidPos);
            cone_pose_unrotated.data.push_back(centroid[0]); //x
            cone_pose_unrotated.data.push_back(centroid[1]); //y
            cone_counter++;
        }

        centroidCloud2.width = centroidCloud2.points.size();
        centroidCloud2.height = 1;
        centroidCloud2.is_dense = true;
        centroidCloud2.header.frame_id = "velodyne"; // important so rviz can do the correct transform
        
    }
    //////////////////////////////////////////////////////////////////////
    pcl::PointCloud<pcl::PointXYZ> output;
    output = centroidCloud2;
    pub_cloud.publish(output);
    cone_pose_unrotated.data[0] = cone_counter;
    pub_array.publish(cone_pose_unrotated);
  }

private:
  ros::NodeHandle n_;
  ros::Publisher pub_cloud, pub_array;
  ros::Subscriber sub_;

}; //End of class SubscribeAndPublish

int main(int argc, char **argv)
{
    ros::init(argc, argv, "roslidar");

    SubscribeAndPublish SAPObject;


    ros::spin();
    return 0;
}