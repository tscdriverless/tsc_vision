#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/PointStamped.h>
#include <std_msgs/Float32MultiArray.h>
#include <sensor_msgs/NavSatFix.h>
#include <stdlib.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/Imu.h>
#include <math.h> 

#define PI 3.14159265

geometry_msgs::Point ins_pose;
geometry_msgs::Point ins_pose_origin;
tf::Quaternion quat_origin;
std_msgs::Float32MultiArray::Ptr cone_cb(new std_msgs::Float32MultiArray);
std_msgs::Float32MultiArray::Ptr vlp_array(new std_msgs::Float32MultiArray);

geometry_msgs::Point cone_pose_temp;
  //vectornav orientation
tf::Quaternion vn_orientation;
tf::Vector3 xaxis(1,0,0);
float origin_angle = 999;
bool got_origin = false;
bool depth = false;
bool imu = false;
bool pose = false;
bool after_first_iteration = false;
bool vlp = false;

void
ins_pose_cb (const geometry_msgs::PointStampedConstPtr& raw_msg)
{
	ins_pose = raw_msg->point;
  pose = true;
}

void 
cone_depth_cb (const std_msgs::Float32MultiArray::ConstPtr& raw_msg)
{
  cone_cb->data = raw_msg->data;
  depth = true;
}

void
vn_imu_cb (const sensor_msgs::ImuConstPtr& raw_msg)
{
  vn_orientation.setX(raw_msg->orientation.x);
  vn_orientation.setY(raw_msg->orientation.y);
  vn_orientation.setZ(raw_msg->orientation.z);
  vn_orientation.setW(raw_msg->orientation.w);
  imu = true;
}

void
vlp_cb (const std_msgs::Float32MultiArray::ConstPtr& raw_msg)
{
  vlp_array->data = raw_msg->data;
  vlp = true;
}

tf::Vector3 
rotate (float x, float y, tf::Quaternion q, float origin_angle, geometry_msgs::Point ipp){
      tf::Vector3 cone_pose_raw;
      tf::Vector3 cone_pose_rotated;
      cone_pose_raw.setX(x);
      cone_pose_raw.setY(y);
      cone_pose_raw.setZ(0);

      float angle = q.getAngle();
    
      float x_rotated = x * cos (-(angle)+origin_angle) - y * sin (-(angle)+origin_angle);
      float y_rotated = x * sin (-(angle)+origin_angle) + y * cos (-(angle)+origin_angle);

      cone_pose_rotated.setX(x_rotated);
      cone_pose_rotated.setY(y_rotated);
      std::cout << "ORIGIN_ANGLE: " << origin_angle << std::endl;
      std::cout << "Cone X: " << x << "Cone Y: " << y << "Winkel Auto: " << (-(angle)+origin_angle)*(180/3.141) <<  std::endl;
      //rotate raw pose --> negative angle
      return cone_pose_rotated;
}

int main( int argc, char** argv )
{
  ros::init(argc, argv, "points_and_lines");
  ros::NodeHandle n;
  ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>("visualization_marker", 10);
  ros::Subscriber sub_ins_pose = n.subscribe<geometry_msgs::PointStamped>("/vectornav/POS_UTM",1, ins_pose_cb);
  ros::Subscriber sub_cone_depth = n.subscribe<std_msgs::Float32MultiArray>("/map",1, cone_depth_cb);		
  ros::Subscriber sub_imu = n.subscribe<sensor_msgs::Imu> ("/vectornav/IMU", 1, vn_imu_cb);
  ros::Subscriber sub_vlp = n.subscribe<std_msgs::Float32MultiArray> ("/vlp16_unrotated", 1, vlp_cb);
  static tf::TransformBroadcaster br;
  tf::Transform transform;
  ros::Rate r(10);

  visualization_msgs::Marker line_strip, line_list, blue, yellow, orangebig, orangesmall;

  while (ros::ok())
  {
  if(depth && imu && pose){
    visualization_msgs::Marker points, direction, lidar;

    geometry_msgs::Point ins_pose_pers = ins_pose; 
    tf::Quaternion quat_pers = vn_orientation;

    // style

      points.header.frame_id = line_strip.header.frame_id = line_list.header.frame_id =  "vectornav";
      lidar.header.frame_id = "vectornav";
      points.header.stamp = line_strip.header.stamp = line_list.header.stamp = lidar.header.stamp = ros::Time::now();
      points.ns = line_strip.ns = line_list.ns = lidar.ns = "points_and_lines";
      points.action = line_strip.action = line_list.action = lidar.action = visualization_msgs::Marker::ADD;
      points.pose.orientation.w = line_strip.pose.orientation.w = line_list.pose.orientation.w =  lidar.pose.orientation.w = 1.0;
      points.id = 0;
      line_strip.id = 1;
      line_list.id = 2;
      lidar.id = 7;

      points.type = visualization_msgs::Marker::POINTS;
      line_strip.type = visualization_msgs::Marker::LINE_STRIP;
      line_list.type = visualization_msgs::Marker::LINE_LIST;
      lidar.type = visualization_msgs::Marker::POINTS;

      // POINTS markers use x and y scale for width/height respectively
      points.scale.x = 0.5;
      points.scale.y = 0.5;

      // LINE_STRIP/LINE_LIST markers use only the x component of scale, for the line width
      line_strip.scale.x = 0.05;
      line_list.scale.x = 0.05;

      // Points are green
      points.color.g = 1.0f;
      points.color.a = 0.5;

      // Line strip is blue
      line_strip.color.r = 1.0;
      line_strip.color.a = 1.0;

      // Line list is red
      line_list.color.r = 1.0;
      line_list.color.a = 1.0;

      //lidar style
      lidar.scale.x = 0.5;
      lidar.scale.y = 0.5;

      lidar.color.g = 1.0;
      lidar.color.a = 1.0;

    points.points.push_back(ins_pose_pers);
    line_strip.points.push_back(ins_pose_pers);

    marker_pub.publish(points);
    marker_pub.publish(line_strip);
    // style
      blue.header.frame_id = yellow.header.frame_id = orangebig.header.frame_id = orangesmall.header.frame_id = direction.header.frame_id = "vectornav";
      blue.header.stamp = yellow.header.stamp = orangebig.header.stamp = orangesmall.header.stamp = direction.header.stamp = ros::Time::now();
      blue.ns = yellow.ns = orangebig.ns = orangesmall.ns = direction.ns = "points_and_lines";
      blue.action = yellow.action = orangebig.action = orangesmall.action = direction.action =  visualization_msgs::Marker::ADD;
      blue.pose.orientation.w = yellow.pose.orientation.w = orangebig.pose.orientation.w = orangesmall.pose.orientation.w = 1.0;
      blue.type = visualization_msgs::Marker::POINTS;
      yellow.type = visualization_msgs::Marker::POINTS;
      orangebig.type = visualization_msgs::Marker::POINTS;
      orangesmall.type = visualization_msgs::Marker::POINTS;
      blue.scale.x = 0.2;
      blue.scale.y = 0.2;
      yellow.scale.x = 0.2;
      yellow.scale.y = 0.2;
      orangebig.scale.x = 0.2;
      orangebig.scale.y = 0.2;
      orangesmall.scale.x = 0.2;
      orangesmall.scale.y = 0.2;
      blue.color.b = 1.0f;
      blue.color.a = 1.0;
      yellow.color.r = 1.0f;
      yellow.color.g = 1.0f;
      yellow.color.a = 1.0;
      orangebig.color.r = 1.0f;
      orangebig.color.g = 0.5f;
      orangebig.color.a = 1.0;
      orangesmall.color.r = 1.0f;
      orangesmall.color.g = 0.5f;
      orangesmall.color.a = 1.0;
      blue.id = 3;
      yellow.id = 4;
      orangebig.id = 5;
      orangesmall.id = 6;
          
    
    std_msgs::Float32MultiArray::Ptr cone_array = cone_cb;
    /*std_msgs::Float32MultiArray::Ptr lidar_array = vlp_array;

    int nb_lidar_cones = lidar_array->data[0];
    std::cout << "LIDAR: " << nb_lidar_cones << std::endl;


    for(int i = 1;i<=nb_lidar_cones+1; i = i+2){
      tf::Vector3 vec_cone_pose_rotated = rotate(lidar_array->data[i], -lidar_array->data[i+1], quat_pers, origin_angle, ins_pose_pers);
      cone_pose_temp.x = lidar_array->data[i];
      cone_pose_temp.y = -lidar_array->data[i+1];
      cone_pose_temp.z = 0;
      lidar.points.push_back(cone_pose_temp);
    }
*/
    int nb_cones = cone_array->data[0];

    for(int i = 1;i<=3*nb_cones+1;i=i+4){
      if(cone_array->data[i+3] > 0.0) {
        if(cone_array->data[i+2]==0){
          cone_pose_temp.x = cone_array->data[i];
          cone_pose_temp.y = cone_array->data[i+1];
          cone_pose_temp.z = 0;
          blue.points.push_back(cone_pose_temp);
        } else if(cone_array->data[i+2]==1){
          cone_pose_temp.x = cone_array->data[i];
          cone_pose_temp.y = cone_array->data[i+1];
          cone_pose_temp.z = 0;
          yellow.points.push_back(cone_pose_temp);
        } else if(cone_array->data[i+2]==2){
          cone_pose_temp.x = cone_array->data[i];
          cone_pose_temp.y = cone_array->data[i+1];
          cone_pose_temp.z = 0;
          orangebig.points.push_back(cone_pose_temp);
        } else if(cone_array->data[i+2]==3){
          cone_pose_temp.x = cone_array->data[i];
          cone_pose_temp.y = cone_array->data[i+1];
          cone_pose_temp.z = 0;
          orangesmall.points.push_back(cone_pose_temp);
        }
      }
    }
      marker_pub.publish(blue);
      marker_pub.publish(yellow);
      marker_pub.publish(orangebig);
      marker_pub.publish(orangesmall);
      marker_pub.publish(lidar);
  } 
    r.sleep();
    ros::spinOnce();
  }
  }


