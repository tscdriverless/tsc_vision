#include <ros/ros.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/MultiArrayLayout.h>
#include <std_msgs/MultiArrayDimension.h>
#include <stdio.h>
#include <stdlib.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CameraInfo.h>
#include <iostream>
#include <fstream>
#include <tinyxml2.h>
#include <string>

int
main (int argc, char** argv)
{
ros::init(argc, argv, "talker");
  std::string file_name;
  std::string image_name;
  const char* file_name_temp;
  const char* image_name_temp;
  tinyxml2::XMLDocument label_doc;
  ROS_INFO("Init done");
  for(int i = 0;i<9000;i++){
    if(i<10){
      file_name = "/home/tsc/2/data/frame000" + std::to_string(i) + ".xml";
      file_name_temp = file_name.c_str();
      image_name = "frame000" + std::to_string(i) + ".jpg";
      image_name_temp = image_name.c_str();
    } else if(i<100){
      file_name = "/home/tsc/2/data/frame00" + std::to_string(i) + ".xml";
      file_name_temp = file_name.c_str(); 
      image_name = "frame00" + std::to_string(i) + ".jpg";
      image_name_temp = image_name.c_str();
    } else if(i<1000){
      file_name = "/home/tsc/2/data/frame0" + std::to_string(i) + ".xml"; 
      file_name_temp = file_name.c_str(); 
      image_name = "frame0" + std::to_string(i) + ".jpg";
      image_name_temp = image_name.c_str(); 
    } else {
      file_name = "/home/tsc/2/data/frame" + std::to_string(i) + ".xml";   
      file_name_temp = file_name.c_str();
      image_name = "frame" + std::to_string(i) + ".jpg";
      image_name_temp = image_name.c_str(); 
    }
    if(label_doc.LoadFile(file_name_temp)==0){
      tinyxml2::XMLNode* root = label_doc.FirstChild();
      tinyxml2::XMLElement* e = root->FirstChildElement("filename");
      e->SetText (image_name_temp);

    std::string cone_blue = "cone-small-blue";
    std::string blue = "blue";
    std::string cone_yellow = "cone-small-yellow";
    std::string yellow = "yellow";

    for (tinyxml2::XMLElement* child = root->FirstChildElement("object"); child!=0; child=child->NextSiblingElement("object"))
    {      

      if(std::strcmp(child->FirstChildElement("name")->GetText(),cone_blue.c_str()) == 0){
        child->FirstChildElement("name")->SetText(blue.c_str());
      } else if(std::strcmp(child->FirstChildElement("name")->GetText(),cone_yellow.c_str()) == 0){
        child->FirstChildElement("name")->SetText(yellow.c_str());
      }
    }
      label_doc.SaveFile(file_name_temp);
    }
  }
    
  
  ros::spin();
}
