#include <ros/ros.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/MultiArrayLayout.h>
#include <std_msgs/MultiArrayDimension.h>
#include <stdio.h>
#include <stdlib.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CameraInfo.h>
#include<iostream>
#include<fstream>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include<cmath>

#define PI 3.14159265

//const
tf::Vector3 xaxis(1,0,0);
tf::Vector3 zaxis(0,0,1);

//globals
geometry_msgs::Point vehicle_pose;
tf::Quaternion quat;
std_msgs::Float32MultiArray cones;
std_msgs::Float32MultiArray map;
std::vector<tf::Vector3> map_blue;
std::vector<tf::Vector3> map_yellow;
std::vector<tf::Vector3> map_orange_big;
std::vector<tf::Vector3> map_orange_small;
std::vector<int> iteration_map_blue;
std::vector<int> iteration_map_yellow;
std::vector<int> iteration_map_orange_big;
std::vector<int> iteration_map_orange_small;

//origin
tf::Vector3 vehicle_pose_origin;
tf::Quaternion quat_origin;
float origin_angle = 710;

//is online?
bool got_pose = false;
bool got_quat = false;
bool got_cones = false;

//origin set?
bool origin_set = false;

//map empty?
bool isempty = true;

// callbacks
void
pose_cb (const geometry_msgs::PointStampedConstPtr& raw_msg)
{
	vehicle_pose = raw_msg->point;
  got_pose = true;
}

void
quaternion_cb (const sensor_msgs::ImuConstPtr& raw_msg)
{
  quat.setX(raw_msg->orientation.x);
  quat.setY(raw_msg->orientation.y);
  quat.setZ(raw_msg->orientation.z);
  quat.setW(raw_msg->orientation.w);
  got_quat = true;
}

void 
cone_cb (const std_msgs::Float32MultiArray::ConstPtr& raw_msg)
{
  cones.data = raw_msg->data;
  got_cones = true;
}

tf::Vector3
rotateToWorld(tf::Vector3 point, float alpha)
{
  float x_rotated = point.getX() * cos (alpha) - point.getY() * sin (alpha);
  float y_rotated = point.getX() * sin (alpha) + point.getY() * cos (alpha);
  tf::Vector3 rotated_point(x_rotated,y_rotated,0);
  std::cout << "X: " << point.getX() << " Y: " << point.getY() << " Alpha: " << alpha*(180/3.141) << std::endl;
  return rotated_point;
}

void
computeWorld()
{
  map.data.clear();
  map.data.push_back(map_blue.size() + map_yellow.size() + map_orange_big.size() + map_orange_small.size());

  for(std::vector<tf::Vector3>::iterator it = map_blue.begin(); it != map_blue.end(); it = it+1) {
    tf::Vector3 cone_temp = *it;
    float x = cone_temp.getX();
    float y = cone_temp.getY();
    map.data.push_back(x);
    map.data.push_back(y);
    map.data.push_back(0.0);
    map.data.push_back(1.0);
  }
  for(std::vector<tf::Vector3>::iterator it = map_yellow.begin(); it != map_yellow.end(); it = it+1) {
    tf::Vector3 cone_temp = *it;
    float x = cone_temp.getX();
    float y = cone_temp.getY();
    map.data.push_back(x);
    map.data.push_back(y);
    map.data.push_back(1.0);
    map.data.push_back(1.0);
  }
  for(std::vector<tf::Vector3>::iterator it = map_orange_big.begin(); it != map_orange_big.end(); it = it+1) {
    tf::Vector3 cone_temp = *it;
    float x = cone_temp.getX();
    float y = cone_temp.getY();
    map.data.push_back(x);
    map.data.push_back(y);
    map.data.push_back(2.0);
    map.data.push_back(1.0);
  }
  for(std::vector<tf::Vector3>::iterator it = map_orange_small.begin(); it != map_orange_small.end(); it = it+1) {
    tf::Vector3 cone_temp = *it;
    float x = cone_temp.getX();
    float y = cone_temp.getY();
    map.data.push_back(x);
    map.data.push_back(y);
    map.data.push_back(3.0);
    map.data.push_back(1.0);
  }
}

int
findNearestNeighbour(std::vector<tf::Vector3> base, tf::Vector3 tba)
{
  tf::Vector3 base_cone;
  int index;
  float best = 0;
  int best_index = -1;
  int base_counter = 0;

  for(std::vector<tf::Vector3>::iterator it_base = base.begin(); it_base != base.end(); ++it_base) {
    base_cone = *it_base;
    if(best < base_cone.distance(tba) && 1.5 > base_cone.distance(tba)){
      best = base_cone.distance(tba);
      best_index = base_counter;
      std::cout << "basesize: " << base.size() << " DEBUG NEIGHBOUR - " << "X: " << base_cone.getX() << " " << tba.getX() << " Y: " << base_cone.getY() << " " << tba.getY() << " Dist: " << base_cone.distance(tba) << " Bestindex: " << best_index << std::endl;
    }
    base_counter++;   
  }
  return best_index;
}

tf::Vector3
computeMean(tf::Vector3 base, tf::Vector3 add, int index, int color)
{
  tf::Vector3 mean_cone;
  std::vector <int> iterations;

  if(color == 0){
    iterations = iteration_map_blue;
  } else if(color == 1){
    iterations = iteration_map_yellow;
  } else if(color == 2){
    iterations = iteration_map_orange_big;
  } else if(color == 3){
    iterations = iteration_map_orange_small;
  }
  int n = iterations.at(index) + 2;
  mean_cone.setX((1.0/n)*add.getX() + ((n-1.0)/n)*base.getX());
  mean_cone.setY((1.0/n)*add.getY() + ((n-1.0)/n)*base.getY());
  mean_cone.setZ(0);

  if(color == 0){
    iteration_map_blue.at(index) = iteration_map_blue.at(index) + 1;
  } else if(color == 1){
    iteration_map_yellow.at(index) = iteration_map_yellow.at(index) + 1;
  } else if(color == 2){
    iteration_map_orange_big.at(index) = iteration_map_orange_big.at(index) + 1;
  } else if(color == 3){
    iteration_map_orange_small.at(index) = iteration_map_orange_small.at(index) + 1;
  }
  std::cout << "DEBUG COMPUTEMEAN - " << "X: " << base.getX() << " " << add.getX() << " Y: " << base.getY() << " " << add.getY() << " N: " << n << " MEANX: " << mean_cone.getX() << std::endl;
  return mean_cone;
}

int
main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "tsc_vision");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud
  ros::Subscriber sub_pose = nh.subscribe<geometry_msgs::PointStamped>("/vectornav/POS_UTM",1, pose_cb);
  ros::Subscriber sub_cones = nh.subscribe<std_msgs::Float32MultiArray>("/unrotated",1, cone_cb);		
  ros::Subscriber sub_quaternion = nh.subscribe<sensor_msgs::Imu> ("/vectornav/IMU", 1, quaternion_cb);

  // Create a ROS publisher for the output
  ros::Publisher pub_map = nh.advertise<std_msgs::Float32MultiArray>("/map",1);

  // Spin
  ros::Rate r(5);

  while (ros::ok()){

    if(got_pose && got_cones && got_quat) {

      //get persistent data
      std_msgs::Float32MultiArray cones_pers;
      cones_pers.data = cones.data;
      float nb_cones = cones_pers.data[0];
      tf::Quaternion quat_pers = quat;
      tf::Vector3 vehicle_pose_pers(vehicle_pose.x,vehicle_pose.y,0);

      //set origin
      if(!origin_set){
        vehicle_pose_origin = vehicle_pose_pers;
        quat_origin = quat_pers;
        origin_set = true;
      }

      //compute delta to origin
      tf::Vector3 delta_vehicle_pose(vehicle_pose_pers.getX()-vehicle_pose_origin.getX(),vehicle_pose_pers.getY()-vehicle_pose_origin.getY(),0);
      tf::Quaternion delta_vehicle_heading = quat_origin.inverse() * quat_pers;

      //get init angle for correct rotation, after 1m
      if (origin_angle == 710 && delta_vehicle_pose.length() > 1.0 ){
        origin_angle = xaxis.angle(delta_vehicle_pose);
        std::cout << origin_angle <<  std::endl;
      }

      if (origin_angle != 710 ){
        float vehicle_angle = -(delta_vehicle_heading.getAngle()) + origin_angle;

        //not needed
        tf::Vector3 sight_line = xaxis.rotate(zaxis,vehicle_angle);
        tf::Vector3 front_line = sight_line.rotate(zaxis,0.5*PI);

        for(int i = 1;i<4*nb_cones-1;i=i+4){
          if(cones_pers.data[i+3] > 0.0) {
            int index;
            tf::Vector3 cone(cones_pers.data[i], -cones_pers.data[i+1],0);
            tf::Vector3 cone_rotated = rotateToWorld(cone, vehicle_angle);
            tf::Vector3 done(cone_rotated.getX() + delta_vehicle_pose.getX(), cone_rotated.getY() + delta_vehicle_pose.getY(), 0);

            if(cones_pers.data[i+2]==0){
              if(map_blue.empty()){
                std::cout << "pushblue" << std::endl;
                map_blue.push_back(done);
                iteration_map_blue.push_back(0);
              } else {
                index = findNearestNeighbour(map_blue, done);
                std::cout << "Nearest index blue : " << index << std::endl;
                if(index == -1){
                std::cout << "pushblue" << std::endl;
                map_blue.push_back(done);
                iteration_map_blue.push_back(0);
                } else(computeMean(map_blue.at(index), done, index, 0));
              }
            } else if(cones_pers.data[i+2]==1){
              if(map_yellow.empty()){
                std::cout << "pushyellow" << std::endl;
                map_yellow.push_back(done);
                iteration_map_yellow.push_back(0);
              } else {                
                index = findNearestNeighbour(map_yellow, done);
                std::cout << "Nearest index yello : " << index << std::endl;
                if(index == -1) {
                  std::cout << "pushyellow" << std::endl;
                  map_yellow.push_back(done);
                  iteration_map_yellow.push_back(0);
                } else(computeMean(map_yellow.at(index), done, index, 1));
              }
            } else if(cones_pers.data[i+2]==2){
              if(map_orange_big.empty()){
                std::cout << "pushob" << std::endl;
                iteration_map_orange_big.push_back(0);                  
                map_orange_big.push_back(done);
              } else {                
                index = findNearestNeighbour(map_orange_big, done);
                if(index == -1) {
                  map_orange_big.push_back(done);
                  iteration_map_orange_big.push_back(0);                  
                } else(computeMean(map_orange_big.at(index), done, index, 2));
              }
            } else if(cones_pers.data[i+2]==3){
              if(map_orange_small.empty()){
                std::cout << "pushos" << std::endl;
                map_orange_small.push_back(done);
                iteration_map_orange_small.push_back(0);                
              } else {                  
                index = findNearestNeighbour(map_orange_small, done);
                  if(index == -1){
                    map_orange_small.push_back(done);
                    iteration_map_orange_small.push_back(0);
                } else(computeMean(map_orange_small.at(index), done, index, 3));
              }
            }
          }
        }
      }
      for (std::vector<tf::Vector3>::const_iterator i = map_blue.begin(); i != map_blue.end(); ++i) {
      tf::Vector3 temp_cone = *i;
      std::cout << temp_cone.getX() << ' ' << temp_cone.getY() << " | ";
      }
      std::cout << " BLUE "<< std::endl;
      for (std::vector<tf::Vector3>::const_iterator i = map_yellow.begin(); i != map_yellow.end(); ++i){
      tf::Vector3 temp_cone = *i;
      std::cout << temp_cone.getX() << ' ' << temp_cone.getY() << " | ";
      }
      std::cout << " YELLOW "<< std::endl;
      for (std::vector<tf::Vector3>::const_iterator i = map_orange_big.begin(); i != map_orange_big.end(); ++i){
      tf::Vector3 temp_cone = *i;
      std::cout << temp_cone.getX() << ' ' << temp_cone.getY() << " | ";
      }
      std::cout << " ORANGEBIG "<< std::endl;
      for (std::vector<tf::Vector3>::const_iterator i = map_orange_small.begin(); i != map_orange_small.end(); ++i){
      tf::Vector3 temp_cone = *i;
      std::cout << temp_cone.getX() << ' ' << temp_cone.getY() << " | ";
      }
      std::cout << " ORANGESMALL "<< std::endl;
    }
    computeWorld();
    pub_map.publish(map);
    r.sleep();
    ros::spinOnce ();
  }
}
