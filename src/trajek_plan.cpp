#include <ros/ros.h>
#include <std_msgs/Float32MultiArray.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <geometry_msgs/Quaternion.h>
#include <std_msgs/MultiArrayDimension.h>
#include <cmath>

#include <iostream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/array.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

using namespace boost::asio;

//geometry_msgs::Quaternion ori;
geometry_msgs::Vector3 ins_vel;
std_msgs::Float32MultiArray::Ptr cone_cb(new std_msgs::Float32MultiArray);

std_msgs::Float32MultiArray target_pub_data;
std_msgs::Float32MultiArray trajek_pub_data;
std_msgs::Float32MultiArray steer_pub_data;

bool ins_vel_bool = false;
bool dspace_vel_bool = false;
bool cone_bool = false;
bool parameter_bool = false;

float vel; //car velocity
uint32_t dspace_vel[1]; //velocity from dspace
float parameter[3]; //dynamicly setable Parameter for DWA via external node from Laptop or something

// own class server udp

class UDPClient
{
public:
    boost::asio::io_service io_service;
    ip::udp::socket socket;
    ip::udp::endpoint receiver_endpoint;


    UDPClient(int);
    void do_receive();
    void handle_receive(const boost::system::error_code& error, size_t);
	void handler(){
		io_service.stop();
	}
};

UDPClient::UDPClient(int Port)
    : io_service(),
      socket(io_service, ip::udp::endpoint(ip::udp::v4(),Port))
{
	do_receive();
	boost::asio::deadline_timer t(io_service, boost::posix_time::microseconds(100000));
	t.async_wait(boost::bind(&UDPClient::handler,this));
	io_service.run();
	io_service.reset();

}

void UDPClient::do_receive()
{
    socket.async_receive_from(boost::asio::buffer(dspace_vel,4), receiver_endpoint,
                               boost::bind(&UDPClient::handle_receive, this,
                               boost::asio::placeholders::error,
                               boost::asio::placeholders::bytes_transferred));
}

void UDPClient::handle_receive(const boost::system::error_code& error, size_t bytes_transferred)
{	
    //if (!error || error == boost::asio::error::message_size)
        //do_receive();
}

void ins_vel_cb (const geometry_msgs::Vector3StampedConstPtr& raw_msg)
{
	ins_vel = raw_msg->vector;
	if(ins_vel.y != 0) ins_vel_bool = true;
  else if(ins_vel.y == 0) ins_vel_bool = false;

}

void cone_depth_cb (const std_msgs::Float32MultiArray::ConstPtr& raw_msg)
{
	cone_cb->data = raw_msg->data;
	cone_bool = true;
}

void dwa_parameter_cb (const std_msgs::Float32MultiArray::ConstPtr& raw_msg)
{
	parameter[0] = raw_msg->data[0];
	parameter[1] = raw_msg->data[1];
	parameter[2] = raw_msg->data[2];
	parameter_bool = true;
	ROS_INFO("got param");
}

float schwimmwinkel(float radius, float lh)
{
	return asin(lh/radius);
}

float xcircle(float angle, float diameter) //calculate the travel to the side on a circular trajectory
{
	return (-cos(angle)+1)*diameter/2.0;        						
}

float ycircle(float angle, float diameter) //calculate the travel to the front on a circular trajectory
{
    return sin(angle)*diameter/2.0;         						
}

int main( int argc, char** argv )
{
	dspace_vel[0]=1000000;	//init dspace_vel
	
	ros::init(argc, argv, "trajek_plan");
	ros::NodeHandle n;
	ros::Subscriber sub_ins_vel = n.subscribe<geometry_msgs::Vector3Stamped>("/vectornav/Velocity",3, ins_vel_cb);
	ros::Subscriber sub_cone_pos = n.subscribe<std_msgs::Float32MultiArray>("/combined_cones",1, cone_depth_cb);
	ros::Subscriber sub_dwaparameter_pos = n.subscribe<std_msgs::Float32MultiArray>("/DWAParameter",1, dwa_parameter_cb);
	ros::Publisher pub_target = n.advertise<std_msgs::Float32MultiArray>("/target",10);
	ros::Publisher pub_trajek = n.advertise<std_msgs::Float32MultiArray>("/trajek_visualisation",10);
	ros::Publisher pub_steer = n.advertise<std_msgs::Float32MultiArray>("/steeringangle",1);
		
		
  	ros::Rate ros_rate(4);

//_________________________UDP________________________BEGIN
	boost::asio::io_service io_service;
 	//erstellt einen Sockel für die Kommunikation
 	boost::asio::ip::udp::socket socket(io_service);

	//erstellt eine Variable in die der Empfänger geschrieben wird
	boost::asio::ip::udp::endpoint remote_endpoint, remote_endpoint_env, remote_endpoint_dwa;
 
	// legt Empfänger fest
	remote_endpoint_dwa = ip::udp::endpoint(ip::address::from_string("192.168.0.47"),7101);
	remote_endpoint_env = ip::udp::endpoint(ip::address::from_string("192.168.0.47"),7102);
	remote_endpoint = ip::udp::endpoint(ip::address::from_string("192.168.0.75"),7103); // 0.0.0.0 = IP, 9000 = PORT -> choose what you want
	
	//öffnet den Sockel
	socket.open(ip::udp::v4());

	//Error Variable
	boost::system::error_code err;
//_________________________UDP________________________END

	float desvel;
	float desrot;
	float posvisx[10];
	float posvisy[10];
	
	//def target
	float tarx=0;
	float tary=1;

	//DWA Variables
	uint8_t vstep=10; //translational steps
	uint8_t rstep=1; //rotational steps
	uint8_t ts=4; //timestep
	uint8_t vtm=2; //maximum translational speed
	float atm=0.5; //maximum translational accelaration
	float wb=1.53; //wheelbase
	float track=1.2; // width of the car (spurbreite)
	float floatingangle; // 
	uint8_t sam=23; //maximum tire to vehicleangle
	uint8_t vrm=sam; //maximum rotational speed changed to steering angle,  may be removed in further version prior to redundancy
	float maxval=0.001; //used to save the maximum value of the dwin
	float alpha, beta, gamma; // parameter for DWA (heading, distance, velocity)
	
	//Environment variables
	int h=10; //height of environment
	int w=10; //width of environment
	int inc=10; // increment per meter
	int hc=5; // amount of helpercones
	float env[h*inc][w*inc]; //environment
	int declarehelp=0; //declaration helper
	
	//prealocations
	float head[vtm*vstep][2*vrm*rstep+1];
	float dwin[vtm*vstep][2*vrm*rstep+1];
	bool nocolision[vtm*vstep][2*vrm*rstep+1];
	float dist[vtm*vstep][2*vrm*rstep+1];
	float velcur[vtm*vstep];
	float orit;
	float vmax, vmin, amax, amin;
	float at, uw, dw, bet, dk, s, sb, dmid, alphahead, alphaheadb, tb;
	int iwe, iweb; // index for width Environment
	int ihe, iheb; //index for height Environment
	
	for(int itvel=0; itvel<vtm*vstep; itvel++)
    {
		velcur[itvel]=(1.0*itvel/vstep)/vtm; //define velocity curve for DWA
    }
	
	

    while (ros::ok())
    {
        ros::spinOnce();
    
    	UDPClient udpclient(7105); //get dspace vel
    	
    	if(dspace_vel[0]!=1000000)
		{
			dspace_vel_bool=true;
			ROS_INFO("got dSpace Vel");
		}	
    	
    		        

        ins_vel_bool = true;
        if ((ins_vel_bool || dspace_vel_bool) && cone_bool)
        {
        	vel=0;
        	if(dspace_vel_bool)
			{
	    		vel = dspace_vel[0]/1000.0;
	    		dspace_vel[0]=1000000;
	    		dspace_vel_bool=false;
			}else{
				if(ins_vel_bool)
		    	{	
					/*vel = sqrt(ins_vel.x*ins_vel.x + ins_vel.y*ins_vel.y);*/vel = 1;
				}
			}
		
			int dimb=0;	//amount of blue cone
			int dimy=0; //amount of yellow cones
			int dimo=0; //amount of orange cones
			int coam=30; //allocation amount for cone arrays
				
			//cone array allocation
			float dataxb[coam];
			float datayb[coam];
			float dataxy[coam];
			float datayy[coam];
			float dataxo[coam];
			float datayo[coam];

			if ((int)cone_cb->data[0]>coam)
				cone_cb->data[0]=coam;
			if ((int)cone_cb->data[0]<=0)
				cone_cb->data[0]=0;

			bool firstblue=true; //true until blue cone is detected
			bool firstyellow=true;
			for(int i=1 ; i<=(int)cone_cb->data[0] ; i++)
			{
				float y=cone_cb->data[i*4-3];
				float x=cone_cb->data[i*4-2];
				if (cone_cb->data[i*4-1]==0)
				{
					if(firstblue)
					{
						dimb++;
						dataxb[dimb-1]=-1.5;
						datayb[dimb-1]=0.5;
						firstblue=false;
					}
					if (y<2 && x<1.5 && x>-1.5)
					{
						dimb++;
						if(x<0)
						{
							dataxb[dimb-1]=-1.5;
						}else{
							dataxb[dimb-1]=1.5;
						}
						datayb[dimb-1]=y;
					}else{
						dimb++;
						dataxb[dimb-1]=x;
						datayb[dimb-1]=y;
					}
				}else{
					if(cone_cb->data[i*4-1]==1)
					{
						if(firstyellow)
						{
							dimy++;
							dataxy[dimy-1]=1.5;
							datayy[dimy-1]=0.5;
							firstyellow=false;
						}
						dimy++;
						dataxy[dimy-1]=x;
						datayy[dimy-1]=y;
					}else{
						if(cone_cb->data[i*4-1]==4)
						{
							if (y>2 && y<7 && x<2.5 && x>-2.5)
							{
								dimo++;
								dataxo[dimo-1]=x;
								datayo[dimo-1]=y;
							}
						}else{
							dimo++;
							dataxo[dimo-1]=x;
							datayo[dimo-1]=y;							
						}
					}
				}
			}	
				
			//calculate lenght to origin
			//blue
			float lb[coam];
			for (int ib=0; ib<dimb; ib++)
			{
				lb[ib]=sqrt(dataxb[ib]*dataxb[ib]+datayb[ib]*datayb[ib]);
			}
			//yellow
			float ly[coam];
			for (int iy=0; iy<dimy; iy++)
			{
				ly[iy]=sqrt(dataxy[iy]*dataxy[iy]+datayy[iy]*datayy[iy]);
			}
			//orange
			float lo[coam];
			for (int io=0; io<dimo; io++)
			{
				lo[io]=sqrt(dataxo[io]*dataxo[io]+datayo[io]*datayo[io]);
			}
			//find biggest cone ammount
			int maxl;
			if(dimb> dimy){
				maxl=dimb;
			}else{
				maxl=dimy;
			}

			//sort algorithm
			int ls=0; //save variable for lenght
			float xs=0; //save variable for x
			float ys=0; //save variable for y
			for (int wdh=0; wdh<maxl; wdh++) //loop with maximum complexity
			{
				if (dimb>wdh)
				{
					for (int is=0; is<dimb-1; is++)
					{
						if(lb[is]>lb[is+1])
						{
							ls=lb[is];
							lb[is]=lb[is+1];
							lb[is+1]=ls;
							xs=dataxb[is];
							dataxb[is]=dataxb[is+1];
							dataxb[is+1]=xs;
							ys=datayb[is];
							datayb[is]=datayb[is+1];
							datayb[is+1]=ys;
						}
					}	
				}
				if (dimy>wdh)
				{
					for (int is=0; is<dimy-1; is++)
					{
						if(ly[is]>ly[is+1])
						{
							ls=ly[is];
							ly[is]=ly[is+1];
							ly[is+1]=ls;
							xs=dataxy[is];
							dataxy[is]=dataxy[is+1];
							dataxy[is+1]=xs;
							ys=datayy[is];
							datayy[is]=datayy[is+1];
							datayy[is+1]=ys;							
						}
					}	
				}						
			}

			//set target
			//tarx=0;
			//tary=1;
			int ang;
			if (dimb!=0 && dimy!=0)
			{
				if (sqrt(dataxb[dimb-1]*dataxb[dimb-1]+datayb[dimb-1]*datayb[dimb-1]-(dataxy[dimy-1]*dataxy[dimy-1]+datayy[dimy-1]*datayy[dimy-1]))<6)
				{
					tarx=(dataxb[dimb-1]-dataxy[dimy-1])*0.5+dataxy[dimy-1];
					tary=(datayb[dimb-1]-datayy[dimy-1])*0.5+datayy[dimy-1];
				}else{
					if(dimb>2)
					{
						ang=atan2(datayb[dimb-1]-datayb[dimb-2],dataxb[dimb-1]-dataxb[dimb-2]);
						tarx=sin(ang)*1.5+dataxb[dimb-1];
						tary=-cos(ang)*1.5+datayb[dimb-1];
						ROS_INFO("BLAUES TARGET");
					}else{
						if(dimy>2)
						{
							ang=atan2(datayy[dimy-1]-datayy[dimy-2],dataxy[dimy-1]-dataxy[dimy-2]);
							tarx=dataxy[dimy-1]-sin(ang)*1.5;
							tary=datayy[dimy-1]+cos(ang)*1.5;
							ROS_INFO("GELBES TARGET");
						}
					}
				}
			}else{
				if(dimb>1)
				{
					ang=atan2(datayb[dimb-1]-datayb[dimb-2],dataxb[dimb-1]-dataxb[dimb-2]);
					tarx=sin(ang)*1.5+dataxb[dimb-1];
					tary=-cos(ang)*1.5+datayb[dimb-1];
					ROS_INFO("BLAUES TARGET 2");
				}else{
					if(dimy>1)
					{
						ang=atan2(datayy[dimy-1]-datayy[dimy-2],dataxy[dimy-1]-dataxy[dimy-2]);
						tarx=dataxy[dimy-1]-sin(ang)*1.5;
						tary=datayy[dimy-1]+cos(ang)*1.5;
						ROS_INFO("GELBES TARGET 2");
					}
				}
			}	

			//generate helper cones
				
			declarehelp=0; //declaration helper
				
			if (dimb>0)
			{
				declarehelp=(((dimb-1)*hc)+dimb);
			}else{
				declarehelp=1;
			}

			float hbx[declarehelp];
			float hby[declarehelp];

			if (dimy>0)
			{
				declarehelp=(((dimy-1)*hc)+dimy);
			}else{
				declarehelp=1;
			}				
			
			float hyx[declarehelp];
			float hyy[declarehelp];
			
			//generate blue hc
			for (int ihb=0; ihb<dimb; ihb++)
			{
				hbx[ihb*(hc+1)]=dataxb[ihb];
				hby[ihb*(hc+1)]=datayb[ihb];

				if(ihb<(dimb-1))
				{
					for(int ih=0; ih<hc; ih++)
					{
						hbx[ihb*(hc+1)+ih+1]=(dataxb[ihb+1]-dataxb[ihb])/(hc+1)*(ih+1)+dataxb[ihb];
						hby[ihb*(hc+1)+ih+1]=(datayb[ihb+1]-datayb[ihb])/(hc+1)*(ih+1)+datayb[ihb];
					}
				}
			}

			//generate yellow hc
			for (int ihy=0; ihy<dimy; ihy++)
			{
				hyx[ihy*(hc+1)]=dataxy[ihy];
				hyy[ihy*(hc+1)]=datayy[ihy];

				if(ihy<(dimy-1))
				{
					for(int ih=0; ih<hc; ih++)
					{
						hyx[ihy*(hc+1)+ih+1]=(dataxy[ihy+1]-dataxy[ihy])/(hc+1)*(ih+1)+dataxy[ihy];
						hyy[ihy*(hc+1)+ih+1]=(datayy[ihy+1]-datayy[ihy])/(hc+1)*(ih+1)+datayy[ihy];
					}
				}
			}

			/*for(int irzero=0; irzero<2*vrm*rstep+1; irzero++)
			{
				for(int itzero=0; itzero<vtm*vstep+1; itzero++)
					{
						env[itzero][irzero]=0;
					}
			}*/
			float maxdis=1.3; //maximum distance after that env is 1
			float mindis=0.6; //maximum distance after that env is 1
			float distance=0;

			//check if there are cones
			if(dimy!=0 || dimb!=0 || dimo!=0)
			{	
				//set all env pointsdepending on coneposition
				for(int iw=0; iw<w*inc; iw++)
				{
					for(int ih=0; ih<h*inc; ih++)
					{
						env[ih][iw]=1;
						int ibenv=0;
						if(dimb!=0)
						{
							while(env[ih][iw]!=0 && ibenv<(((dimb-1)*hc)+dimb))
							{
								distance=sqrt(((hbx[ibenv]-((1.0*iw/inc)-(w/2.0)))*(hbx[ibenv]-((1.0*iw/inc)-(w/2.0))))+((hby[ibenv]-(1.0*ih/inc))*(hby[ibenv]-(1.0*ih/inc))));
								if(distance<mindis)
								{
									env[ih][iw]=0;
								}else{
									if(distance<maxdis && (1-((maxdis-distance)/(maxdis-mindis)))<env[ih][iw])
									{
										env[ih][iw]=1-((maxdis-distance)/(maxdis-mindis));
									}
								}
								ibenv++;
							}
						}
						int iyenv=0;
						if(dimy!=0)
						{						
							while(env[ih][iw]!=0 && iyenv<(((dimy-1)*hc)+dimy))
							{
								distance=sqrt(((hyx[iyenv]-((1.0*iw/inc)-(w/2.0)))*(hyx[iyenv]-((1.0*iw/inc)-(w/2.0))))+((hyy[iyenv]-(1.0*ih/inc))*(hyy[iyenv]-(1.0*ih/inc))));
								if(distance<mindis)
								{
									env[ih][iw]=0;
								}else{
									if(distance<maxdis && (1-((maxdis-distance)/(maxdis-mindis)))<env[ih][iw])
									{
										env[ih][iw]=1-((maxdis-distance)/(maxdis-mindis));
									}
								}
								iyenv++;
							}
						}
						int ioenv=0;
						if(dimo!=0)
						{
							while(env[ih][iw]!=0 && ioenv<dimo)
							{
								distance=sqrt(((dataxo[ioenv]-((1.0*iw/inc)-(w/2.0)))*(dataxo[ioenv]-((1.0*iw/inc)-(w/2.0))))+((datayo[ioenv]-(1.0*ih/inc))*(datayo[ioenv]-(1.0*ih/inc))));
								if(distance<mindis)
								{
									env[ih][iw]=0;
								}else{
									if(distance<maxdis && (1-((maxdis-distance)/(maxdis-mindis)))<env[ih][iw])
									{
										env[ih][iw]=1-((maxdis-distance)/(maxdis-mindis));
									}
								}
								ioenv++;
							}
						}
					}
				}
			}
						

			//calculate the angle to target
			orit=acos(tary/(sqrt(tarx*tarx+tary*tary)));

			if(tarx<0)
			{
				orit=-orit;
			}

			for(int irzero=0; irzero<2*vrm*rstep+1; irzero++)
			{
				for(int itzero=0; itzero<vtm*vstep; itzero++)
				{
						dist[itzero][irzero]=0;
						head[itzero][irzero]=0;
						nocolision[itzero][irzero]=false;
						dwin[itzero][irzero]=0;
				}
			}
			
			int ir=0;
			for(int r=-vrm; r<=vrm; r++) //r equals the tireangle
			{
				//vmin=floor(sqrt(((2*r*wb)/(360*sin(sam*M_PI/180)))*((2*r*wb)/(360*sin(sam*M_PI/180))))*vstep)/vstep;
				amax=(vtm-vel)/ts;
				if(amax>atm)
				{
					vmax=floor((atm*ts+vel)*vstep)/vstep;
				}else{
					vmax=vtm;
				}
					
				amin=(-vel)/ts;
				if(amin<-atm)
				{
					vmin=floor((-atm*ts+vel)*vstep)/vstep;
				}else{
					vmin=0;
				}
					
				int it = (vmin*vstep);
				if(vmin<vmax)
				{
					float t=(1.0/vstep); //speed translational for iteration

					bool firstdist=true;
					bool distcolision=false;
						
					while(t<=vtm && !distcolision)
					{
						if(vmin<=t && vmax>=t)
						{
							
							//calculation based on iteration over all tireangles
							at=(t-vel)/ts; //calculate acceleration
							s=(at/2*ts*ts+vel*ts); //travel of the Car
							
							if (r!=0) //avoid 1/0=nan
								dmid=(2.4*wb/sin(r*M_PI/180.0))-(track*(r/sqrt(r*r))); // diameter of the circle based on the tireangle/ 2.4 is normaly 2.0 but the car is doing less (2,4 measured)
							else
								dmid=10000; // straigth line
								
							uw=M_PI*dmid; // travel 1 time aorund the circle
							alphahead=s*2*M_PI/uw; // the heading change
								
							tb=vel/atm; //time to brake to 0 velocity
							sb=(-atm/2*tb*tb+vel*tb); //travel to brake to zero velocity
							alphaheadb=(s+sb)*2*M_PI/uw; // the heading change with braking
								
							head[it][ir]=1-sqrt((-orit+alphahead)*(-orit+alphahead))/(M_PI/2); //set heading curve
								
							if(head[it][ir]<0)
							{
								head[it][ir]=0;
							}
							
							floatingangle=schwimmwinkel(dmid/2,0.758);
							//calculate indices for environment comparison
							if (r==0)
							{
								iwe=round((w/2.0)*inc);
								ihe=round((s)*inc);
								//iweb=round((w/2.0)*inc);
								//iheb=round((sb+s)*inc);
							}else{
								iwe=round((((xcircle(alphahead+floatingangle,dmid))-xcircle(floatingangle,dmid))+(w/2.0))*inc)-1;//width index based on calculated position in x without braking
								ihe=round(((ycircle(alphahead+floatingangle,dmid))-ycircle(floatingangle,dmid))*inc)-1;//heigth index based on calculated position in y without braking
								//iweb=round((((xcircle(alphaheadb+floatingangle,dmid))-xcircle(floatingangle,dmid))+(w/2.0))*inc)-1;//width index based on calculated position in x with braking
								//iheb=round(((ycircle(alphaheadb+floatingangle,dmid))-ycircle(floatingangle,dmid))*inc)-1;//heigth index based on calculated position in y with braking
							}
							if (ihe<h*inc && iwe<w*inc && iwe>=0 && ihe>=0)
							{
								if(env[ihe][iwe]>0)
								{
									if(firstdist)
									{
										nocolision[it][ir]=true;
										firstdist=false;
										dist[it][ir]=env[ihe][iwe];
									}else{
											nocolision[it][ir]=true;

											if(env[ihe][iwe]<dist[it-1][ir])
											{
												dist[it][ir]=env[ihe][iwe];
											}else{
												dist[it][ir]=dist[it-1][ir];
											}
									}
								}else{
									distcolision=true;
								}	
							}
							it++;
						}					
						t=t+(1.0/vstep);
					}
					
				}
				ir++;
			}

			//combining DWA Curve
			//define coeficients first
			if (!parameter_bool)
			{
				alpha=0.1;
				beta=1;
				gamma=0.1;
			}else{
				alpha=parameter[0];
				beta=parameter[1];
				gamma=parameter[2];
				parameter_bool=false;
			}
			//ROS_INFO("%f",alpha);

			int maxt=0; // zero velocity
			int maxr=vrm*rstep+1; //middle position if no Data
			maxval=0.0001; //maximum value
			for(int t=0; t<vtm*vstep; t++)
			{
				for(int r=0; r<2*vrm*rstep+1; r++)
				{
					if(nocolision[t][r])
					{
						dwin[t][r]=alpha*head[t][r]+beta*dist[t][r]+gamma*velcur[t];
					}
						
					if (dwin[t][r]>maxval)
					{
						maxt=t;
						maxr=r;
						maxval=dwin[t][r];
					}
				}
			}

			desvel = 1.0*maxt/vstep;
			desrot = 1.0*maxr/rstep-vrm;
			//ROS_INFO("%f",desrot*4);

			float v=desvel;
			float rota=desrot;
				
			at=(v-vel)/ts; //calculate acceleration
								
			if (rota!=0) //avoid 1/0=nan
				dmid=(2*wb/sin(rota*M_PI/180))-track; // diameter of the circle based on the tireangle
			else
				dmid=10000; // straigth line
					
			uw=M_PI*dmid;

			floatingangle=schwimmwinkel(dmid/2,0.758);
			float minenv=1;
					
			for(int ipos=1; ipos<=10; ipos++)
			{			
				s=(at/2*(ts/10.0*ipos)*(ts/10.0*ipos)+vel*(ts/10.0*ipos)); //travel of the Car
				alphahead=s*2.0*M_PI/uw; // the heading change
						
				posvisx[ipos-1]=xcircle(alphahead+floatingangle,dmid)-xcircle(floatingangle,dmid);
				posvisy[ipos-1]=ycircle(alphahead+floatingangle,dmid)-ycircle(floatingangle,dmid);

				if (env[(int)(round((posvisy[ipos-1])*inc)-1)][(int)(round(((posvisx[ipos-1])+(w/2.0))*inc)-1)]<minenv)
					minenv=env[(int)(round((posvisy[ipos-1])*inc)-1)][(int)(round(((posvisx[ipos-1])+(w/2.0))*inc)-1)];

					env[(int)(round((posvisy[ipos-1])*inc)-1)][(int)(round(((posvisx[ipos-1])+(w/2.0))*inc)-1)]=0;
			}
			ROS_INFO("%f",minenv);
		
			

			uint32_t send[2];
			send[0]=((desvel+2000)*100);
			send[1]=((desrot+2000)*100);

			steer_pub_data.data.clear();
			steer_pub_data.data.push_back(desrot);
			pub_steer.publish(steer_pub_data);

			//prepare trajekdata
			trajek_pub_data.data.clear();
			for(int i=0; i<20; i++)
			{
				if(i<10)
				{
					trajek_pub_data.data.push_back(posvisx[i]);
				}else{
					trajek_pub_data.data.push_back(posvisy[i-10]);
				}
			}
			pub_trajek.publish(trajek_pub_data);

			//prepare target data
			target_pub_data.data.clear();
			target_pub_data.data.push_back(tary);
			target_pub_data.data.push_back(tarx);
			pub_target.publish(target_pub_data);

			//prepare env data
			float env_send[inc*inc*w*h];
			for(int esh=0; esh<h*inc; esh++)
			{
				for(int esw=0; esw<w*inc; esw++)
				{
					env_send[esw+(w*inc*esh)]=env[esh][esw];
				}
			}
			//prepare dwa data
			float dwa_send[(vtm*vstep)*(2*vrm*rstep+1)];
			for(int dst=0; dst<vtm*vstep; dst++)
			{
				for(int dsr=0; dsr<2*vrm*rstep+1; dsr++)
				{
					dwa_send[dsr+((2*vrm*rstep+1)*dst)]=dwin[dst][dsr]/maxval;
				}
			}		


			//Senden der Daten
			socket.send_to(buffer(send,8), remote_endpoint, 0, err); // 8=4*2=4ByteProVariable*2Variablen
			socket.send_to(buffer(env_send,4*10000), remote_endpoint_env, 0, err);
			socket.send_to(buffer(dwa_send,4*1437), remote_endpoint_dwa, 0, err);
		}
	
		ros_rate.sleep();	
	}
	
	// Schließen des Sockels
	socket.close();
}


