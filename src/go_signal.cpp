/*
 * Copyright (c) 2017, 2018, Eric Gorkow, Team Starcraft e.V., Ilmenau, Germany
 * All rights reserved.
 */

#include <ros/ros.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/PointStamped.h>
#include <sensor_msgs/Image.h>

std_msgs::Bool go;
bool zed = false;
bool nn = false;
bool ins = false;
bool vlp = false; 

void 
zed_cb (const sensor_msgs::ImageConstPtr& raw_msg)
{
  	zed = true;
}

void
ins_cb (const geometry_msgs::PointStampedConstPtr& raw_msg)
{
	ins = true;
}

void 
nn_cb (const std_msgs::Float32MultiArray::ConstPtr& raw_msg)
{
	nn = true;
}

void
vlp_cb (const std_msgs::Float32MultiArray::ConstPtr& raw_msg)
{
 	vlp = true;
}

// Main void
int main( int argc, char** argv )
{
	// Initialize ROS
	ros::init (argc, argv, "go_signal");
	ros::NodeHandle nh;
	ros::Publisher pub = nh.advertise<std_msgs::Bool>("/go", 1);

    ros::Subscriber sub_vlp = nh.subscribe<std_msgs::Float32MultiArray> ("/vlp16_unrotated", 1, vlp_cb);
    ros::Subscriber sub_zed = nh.subscribe<sensor_msgs::Image> ("/zed/depth/depth_registered", 1, zed_cb);
    ros::Subscriber sub_nn = nh.subscribe<std_msgs::Float32MultiArray> ("/bndbxs", 1, nn_cb);
    ros::Subscriber sub_pose = nh.subscribe<geometry_msgs::PointStamped>("/vectornav/POS_UTM",1, ins_cb);

	while(ros::ok()){
		if(zed && nn && ins && vlp){
			//UDP
			go.data = true;
			pub.publish(go);
		}
		ros::spinOnce();
	}
}

