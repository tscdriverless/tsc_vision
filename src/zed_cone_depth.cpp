#include <ros/ros.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/MultiArrayLayout.h>
#include <std_msgs/MultiArrayDimension.h>
#include <stdio.h>
#include <stdlib.h>
#include <nav_msgs/Odometry.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/CameraInfo.h>
#include<iostream>
#include<fstream>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

ros::Publisher pubUnrotated, pubUnrotatedMean, pubCVDepth;

  //camera
float fx = 705.541;
float fy = 705.541;
float cx = 652.228;
float cy = 339.166;
tf::Vector3 zed_pose_raw;
tf::Vector3 zed_pose_rotated;
tf::Quaternion zed_orientation;
sensor_msgs::ImagePtr depth_msg_temp( new sensor_msgs::Image );
cv_bridge::CvImageConstPtr cv_ptr;


  //vectornav orientation
tf::Quaternion vn_orientation;

  //cone array from nn
std_msgs::Float32MultiArray::Ptr cone_msg(new std_msgs::Float32MultiArray);

  //outpout arrays
std_msgs::Float32MultiArray cone_pose_unrotated;
std_msgs::Float32MultiArray cone_pose_unrotated_mean;

  //cb_state
bool nn = false;
bool vn = false;
bool zed = false;

  //get camera parameter
void 
cameraInfo_cb (const sensor_msgs::CameraInfoConstPtr& raw_msg)
{
  fx = raw_msg->P[0]; 
  fy = raw_msg->P[4];
  cx = raw_msg->P[2];
  cy = raw_msg->P[5];
}

  //input from NN 
void 
NnOutput_cb (const std_msgs::Float32MultiArray::ConstPtr& raw_msg)
{
  cone_msg->data = raw_msg->data;
  if(!nn) nn = true;
}

  //zed odometry unrotated pose and rotation
void
zed_odom_cb (const nav_msgs::Odometry::ConstPtr& raw_msg)
{
  zed_pose_raw.setX(raw_msg->pose.pose.position.x);
  zed_pose_raw.setY(raw_msg->pose.pose.position.y);
  zed_pose_raw.setZ(raw_msg->pose.pose.position.z);

  zed_orientation.setX(raw_msg->pose.pose.orientation.x);
  zed_orientation.setY(raw_msg->pose.pose.orientation.y);
  zed_orientation.setZ(raw_msg->pose.pose.orientation.z);
  zed_orientation.setW(raw_msg->pose.pose.orientation.w);
  
}

//Vectornav INS Imu for Orientation
void
vn_imu_cb (const sensor_msgs::ImuConstPtr& raw_msg)
{
  vn_orientation.setX(raw_msg->orientation.x);
  vn_orientation.setY(raw_msg->orientation.y);
  vn_orientation.setZ(raw_msg->orientation.z);
  vn_orientation.setW(raw_msg->orientation.w);
  if(!vn) vn = true;
}

  //coordinate calculation
void 
depth_cb (const sensor_msgs::ImageConstPtr& raw_msg)
{ 
  depth_msg_temp->header   = raw_msg->header;
  depth_msg_temp->height   = raw_msg->height;
  depth_msg_temp->width = raw_msg->width;
  depth_msg_temp->encoding = raw_msg->encoding;
  depth_msg_temp->step     = raw_msg->step;
  depth_msg_temp->data = raw_msg->data;
  if(!zed) zed = true;
}

  //fill cones in array
void
fillConeUnrotated (float dx, float dy, float t, float c, bool mean)
{
  if(mean){
    cone_pose_unrotated_mean.data.push_back(dx);
    cone_pose_unrotated_mean.data.push_back(dy);
    cone_pose_unrotated_mean.data.push_back(t); 
    cone_pose_unrotated_mean.data.push_back(c);
  } else {
    cone_pose_unrotated.data.push_back(dx);
    cone_pose_unrotated.data.push_back(dy);
    cone_pose_unrotated.data.push_back(t); 
    cone_pose_unrotated.data.push_back(c);
  }
}

float
calcDepth(float rect[4],sensor_msgs::ImagePtr depth_image)
{
  float min = 0.0;
  int pxl = (rect[3]-rect[1])*(rect[2]-rect[0]);
  for(int y = rect[1];y<=rect[3];y++){
    for(int x = rect[0];x<=rect[2];x++){
      float temp = *reinterpret_cast<float*>(&depth_image->data[(y-1)*depth_image->step+4*(x-1)]);
      if(min == 0)min=temp;
      if(!std::isnan(temp) && !std::isinf(temp) && temp < min)min = temp;
    }
  }
  return min;
}

int
main (int argc, char** argv)
{
  // Initialize ROS
  ros::init (argc, argv, "tsc_vision");
  ros::NodeHandle nh;

  // Create a ROS subscriber for the input point cloud
  ros::Subscriber sub_0 = nh.subscribe<std_msgs::Float32MultiArray>("/bndbxs", 1, NnOutput_cb);
  ros::Subscriber sub_1 = nh.subscribe<sensor_msgs::CameraInfo> ("/zed/depth/camera_info", 1, cameraInfo_cb);
  ros::Subscriber sub_2 = nh.subscribe<nav_msgs::Odometry> ("/zed/odom", 1, zed_odom_cb);
  ros::Subscriber sub_3 = nh.subscribe<sensor_msgs::Image> ("/zed/depth/depth_registered", 1, depth_cb);
  ros::Subscriber sub_4 = nh.subscribe<sensor_msgs::Imu> ("/vectornav/IMU", 1, vn_imu_cb);

  // Create a ROS publisher for the output
  pubUnrotatedMean = nh.advertise<std_msgs::Float32MultiArray> ("/unrotated_mean", 1);
  pubUnrotated = nh.advertise<std_msgs::Float32MultiArray> ("/unrotated", 1);

  // Spin
  ros::Rate r(10);

  float type;
  float confidence;

  //rect [xmin,ymin,xmax,ymax]
  float rect[4];

  while (ros::ok())
  {
    if(nn && zed){
      sensor_msgs::ImagePtr depth_msg = depth_msg_temp; 
      depth_msg->data.clear(); 
      std_msgs::Float32MultiArray::Ptr temp_msg = cone_msg;   //store msg local
      int nb_cones = (int)temp_msg->data[0];                 //first field in arrays is nb of cones in frame
      cone_pose_unrotated.data.clear();
      cone_pose_unrotated_mean.data.clear();
      cone_pose_unrotated_mean.data.push_back(nb_cones);
      cone_pose_unrotated.data.push_back(nb_cones);
      for(int i = 1;i<temp_msg->data.size();i=i+6){                    //depth calc for every cone 
        rect[0] = temp_msg->data[i];                          //xmin
        rect[1] = temp_msg->data[i+1];                        //ymin
        rect[2] = temp_msg->data[i+2];                        //xmax
        rect[3] = temp_msg->data[i+3];                        //ymax
        type = temp_msg->data[i+4];       
        confidence = temp_msg->data[i+5];   
        
        if(rect[0] == 0)rect[0] = 1;
        if(rect[1] == 0)rect[1] = 1;
        if(rect[2] > 1279)rect[2] = 1279;
        if(rect[3] > 719)rect[3] = 719;
        //center of given rect with img_x1,img_y1,img_x2,img_y2
        int img_x = (rect[2]+rect[0])/2;
        int img_y = (rect[3]+rect[1])/2;

        //actual reinterpret --> depth 32bit float distance
        //1280*720, 4 byte/pixel, depth msg = bytearray, 1280*4 -> 1 step = 5120, 720*step=total length
        //float depth = calcDepth(rect,conversion_mat_);
        float depth_x = calcDepth(rect,depth_msg);
        float depth_y;
        float depth_z;
        //pinhole camera model 
        //calc. matrix
        //   xyz_depth << ((u - depth_cx)*depth - depth_Tx) * inv_depth_fx,
        //                ((v - depth_cy)*depth - depth_Ty) * inv_depth_fy,
        //                depth)
        depth_y = ((img_x - cx) * depth_x)/fx;
        depth_z = ((img_y - cy) * depth_x)/fy;
        fillConeUnrotated(depth_x,depth_y,type,confidence, true);

        depth_x = *reinterpret_cast<float*>(&depth_msg->data[(img_y-1)*depth_msg->step+4*(img_x-1)]);

        if(!std::isnan(depth_x) && !std::isinf(depth_x)){
          depth_y = ((img_x - cx) * depth_x)/fx;
          depth_z = ((img_y - cy) * depth_x)/fy;

          fillConeUnrotated(depth_x,depth_y,type,confidence, false);
        }

      }
      pubUnrotatedMean.publish(cone_pose_unrotated_mean);
      pubUnrotated.publish(cone_pose_unrotated);
    } 
    

    r.sleep();
    ros::spinOnce ();
  }
}
